Author: JT Kashuba
Contact Address: jkashuba@uoregon.edu
Course: CIS 322 Intro to Software Engineering - Fall '20

Description: Trivial project to practice the workflow and user interface with git/bitbucket
and setting up Docker.

Uses a makefile to run a python program. This python program pulls a message from a "hidden"
(to the client, or whomever) file [credentials.ini] and prints it at the command line. Hello world!
